package test.java;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.webapp.model.Artiest;
import com.webapp.service.ArtiestService;

@RunWith(SpringRunner.class)
@ContextConfiguration({"../resources/webapp-servlet.xml"})
public class JUnitHibernateTest {
	@Autowired
	private ArtiestService artiestService;
	
	@Test
	public void testArtiestService() {
		assertEquals(
				"class com.webapp.service.ArtiestServiceImpl",
				this.artiestService.getClass().toString());
	}
	
	@Test
	public void testArtiest(){
		Artiest artiest = artiestService.save("JUnit Test artiest");
		int id = artiest.getId();
		Artiest testArtiest = artiestService.getArtiestById(id);
		assertEquals(artiest.getName(), testArtiest.getName());
		artiestService.deleteById(id);
	}
}
