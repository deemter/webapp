package com.webapp.dao;

import java.util.List;

import com.webapp.model.Artiest;

public interface ArtiestDAO {
	public Artiest save(String name);
	
	public List<Artiest> list();
	
	public List<Artiest> getArtiest(String name);
	
	public Artiest getArtiestById(int id);
	
	public void deleteById(int id);
	
	public void updateById(int id, String name);
}
