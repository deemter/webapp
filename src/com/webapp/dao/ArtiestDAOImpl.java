package com.webapp.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.webapp.model.Artiest;

public class ArtiestDAOImpl implements ArtiestDAO {

	private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
	@Override
	public Artiest save(String name) {
		Artiest artiest = new Artiest();
		artiest.setName(name);
		
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(artiest);
		tx.commit();
		session.close();
		return artiest;
	}
	


	@Override
	public List<Artiest> list() {
		Session session = this.sessionFactory.openSession();
		List<Artiest> artiestList = session.createQuery("from Artiest").list();
		session.close();
		return artiestList;
		
	}
	
	@Override
	public List<Artiest> getArtiest(String name){
		Session session = this.sessionFactory.openSession();
	    Query query = session.createQuery("from Artiest where artiest_naam LIKE :artiest_naam");
	    query.setParameter("artiest_naam", "%"+name+"%");
		return query.list();
	}

	@Override
	public Artiest getArtiestById(int id) {
		Session session = this.sessionFactory.openSession();
	    Query query = session.createQuery("from Artiest where artiest_id = :artiest_id");
	    query.setParameter("artiest_id", id);
		return (Artiest) query.list().get(0);
	}
	
	@Override
	public void deleteById(int artiest_id) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Artiest a = (Artiest) session.load(Artiest.class, new Integer(artiest_id));
		if(a != null){
			session.delete(a);
			tx.commit();
			session.close();
		}
	}
	
	@Override
	public void updateById(int id, String name){
		Session session = this.sessionFactory.openSession();
		
		String hql = "UPDATE Artiest set artiest_naam = :artiest_naam "  + 
	             "WHERE artiest_id = :artiest_id";
		Query query = session.createQuery(hql);
		query.setParameter("artiest_naam", name);
		query.setParameter("artiest_id", id);
		int result = query.executeUpdate();
	}
}
