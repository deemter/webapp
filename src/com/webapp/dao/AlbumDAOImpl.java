package com.webapp.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.webapp.model.Album;
import com.webapp.model.Artiest;
import com.webapp.service.ArtiestService;

public class AlbumDAOImpl implements AlbumDAO {

	private SessionFactory sessionFactory;

	@Autowired
	private ArtiestService artiestService;
	
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
	@Override
	public void save(int artiestId, String name) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Artiest artiest = artiestService.getArtiestById(artiestId);
		Album album = new Album();
		album.setName(name);
		album.setArtiest(artiest);

		session.persist(album);
		tx.commit();
		session.close();	
	} 	

	@Override
	public List<Album> list() {
		Session session = this.sessionFactory.openSession();
		List<Album> albumList = session.createQuery("from Album").list();
		session.close();
		return albumList;
	}

	@Override
	public Album getAlbumById(int id) {
		Session session = this.sessionFactory.openSession();
	    Query query = session.createQuery("from Album where album_id = :album_id");
	    query.setParameter("album_id", id);
	    Album album = (Album) query.list().get(0);
	    session.close();
		return album;
	}

	@Override
	public List<Album> getAlbumByArtiestId(int id) {
		Session session = this.sessionFactory.openSession();
	    Query query = session.createQuery("from Album where artiest_id = :artiest_id");
	    query.setParameter("artiest_id", id);
	    List<Album> a = query.list();
		return a;
	}
	
	@Override
	public void deleteById(int album_id) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();	
		Album a = (Album) session.load(Album.class, new Integer(album_id));
		if(a != null){
			session.delete(a);
			tx.commit();
			session.close();
		}
	}
	
	@Override
	public void updateById(int id, String name){
		Session session = this.sessionFactory.openSession();
		
		String hql = "UPDATE Album set album_naam = :album_naam "  + 
	             "WHERE album_id = :album_id";
		Query query = session.createQuery(hql);
		query.setParameter("album_naam", name);
		query.setParameter("album_id", id);
		int result = query.executeUpdate();
	}
}
