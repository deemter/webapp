package com.webapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.webapp.model.Album;
import com.webapp.model.Artiest;
import com.webapp.service.AlbumService;

@RestController
public class AlbumController {
	
	@Autowired
	private AlbumService albumService;	
	
	// Delete album
	@RequestMapping(value = "/deleteAlbum/{id}", method = RequestMethod.GET)
	public String deleteAlbum(@PathVariable("id") int id) {
		albumService.deleteById(id);
        return "deleted";
	}
	
	// Update album
	@RequestMapping(value = "/updateAlbum/{id}/{name}", method = RequestMethod.GET)
	public String updateAlbum(@PathVariable("id") int id, @PathVariable("name") String name) {
		albumService.updateById(id, name);
		return "updated";
	}
	
	// New album
	@RequestMapping(value = "/createAlbum/{id}/{name}", method = RequestMethod.GET)
	public String createAlbum(@PathVariable("id") int id, @PathVariable("name") String name) {
		albumService.save(id, name);
		return "updated";
	}
	
	// list all albums by artiest
	@RequestMapping(value = "/getAlbum/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Album>> getAlbum(@PathVariable("id") int id) {
        List<Album> albums = albumService.getAlbumByArtiestId(id);
        if(albums.isEmpty()){
            return new ResponseEntity<List<Album>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        System.out.println(albums);
        return new ResponseEntity<List<Album>>(albums, HttpStatus.OK);
	}
}
