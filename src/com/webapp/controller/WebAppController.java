package com.webapp.controller;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.webapp.dao.ArtiestDAO;
import com.webapp.model.Artiest;
 
/*
 * author: Crunchify.com
 * 
 */
 
@RestController
public class WebAppController {
 
	@Autowired
	private ApplicationContext appContext;
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ResponseEntity<List<Artiest>> helloWorld() {
		ArtiestDAO artiestDAO = appContext.getBean(ArtiestDAO.class);
		
        List<Artiest> users = artiestDAO.list();
        if(users.isEmpty()){
            return new ResponseEntity<List<Artiest>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        System.out.println(users);
        return new ResponseEntity<List<Artiest>>(users, HttpStatus.OK);
	}
	
   @RequestMapping(value = "/staticPage")
   public ModelAndView staticPage() {
      return new ModelAndView("static/html/index.html");
   }
   
   @RequestMapping(value = "/#!/static/html/phones")
   public ModelAndView home() {
     
      return new ModelAndView("static/html/index.html");
   }
}