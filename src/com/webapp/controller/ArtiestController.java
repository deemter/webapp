package com.webapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.webapp.dao.ArtiestDAO;
import com.webapp.model.Artiest;
import com.webapp.service.ArtiestService;

@RestController
public class ArtiestController {
	
	@Autowired
	private ArtiestService artiestService;
	
	// Delete artiest
	@RequestMapping(value = "/deleteArtiest/{id}", method = RequestMethod.GET)
	public String deleteArtist(@PathVariable("id") int id) {
		artiestService.deleteById(id);
        return "deleted";
	}
	
	// Update artiest
	@RequestMapping(value = "/updateArtiest/{id}/{name}", method = RequestMethod.GET)
	public String updateArtist(@PathVariable("id") int id, @PathVariable("name") String name) {
		artiestService.updateById(id, name);
        return "updated";
	}
	
	// New artiest
	@RequestMapping(value = "/createArtiest/{name}", method = RequestMethod.GET)
	public String createArtiest(@PathVariable("name") String name) {
		artiestService.save(name);
		return "updated";
	}
	
	// Search artiest
	@RequestMapping(value = "/getArtiest/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<Artiest>> getArtiest(@PathVariable("name") String name) {
        List<Artiest> users = artiestService.getArtiest(name);
        if(users.isEmpty()){
            return new ResponseEntity<List<Artiest>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        System.out.println(users);
        return new ResponseEntity<List<Artiest>>(users, HttpStatus.OK);
		//return new ModelAndView("static/html/index.html");
	}
	
	// list all artiesten
	@RequestMapping(value = "/getArtiest", method = RequestMethod.GET)
	public ResponseEntity<List<Artiest>> getArtiest() {
        List<Artiest> users = artiestService.list();
        if(users.isEmpty()){
            return new ResponseEntity<List<Artiest>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        System.out.println(users);
        return new ResponseEntity<List<Artiest>>(users, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getArtiestById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Artiest> getArtiestById(@PathVariable("id") int id) {
        Artiest users = artiestService.getArtiestById(id);
        return new ResponseEntity<Artiest>(users, HttpStatus.OK);
	}
}
