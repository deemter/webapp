package com.webapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webapp.dao.ArtiestDAO;
import com.webapp.model.Artiest;

@Service("artiestService")
@Transactional
public class ArtiestServiceImpl implements ArtiestService{

	@Autowired
	ArtiestDAO dao;
	
	@Override
	public Artiest save(String name) {
		return dao.save(name);
	}

	@Override
	public List<Artiest> list() {
		return dao.list();
	}

	@Override
	public List<Artiest> getArtiest(String name) {
		return dao.getArtiest(name);
	}

	@Override
	public Artiest getArtiestById(int id) {
		return dao.getArtiestById(id);
	}

	@Override
	public void deleteById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void updateById(int id, String name) {
		dao.updateById(id, name);
	}
}
