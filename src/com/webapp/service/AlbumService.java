package com.webapp.service;

import java.util.List;

import com.webapp.model.Album;

public interface AlbumService {
	public void save(int artiestId, String name);
	
	public List<Album> list();
	
	public Album getAlbumById(int id);
	
	public List<Album> getAlbumByArtiestId(int id);
	
	public void deleteById(int id);
	
	public void updateById(int id, String name);
}
