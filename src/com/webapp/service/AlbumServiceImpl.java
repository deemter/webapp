package com.webapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.webapp.dao.AlbumDAO;
import com.webapp.model.Album;

public class AlbumServiceImpl implements AlbumService {

	@Autowired
	private AlbumDAO dao;
	
	@Override
	public void save(int artiestId, String name) {
		dao.save(artiestId, name);
	}

	@Override
	public List<Album> list() {
		return dao.list();
	}

	@Override
	public Album getAlbumById(int id) {
		return dao.getAlbumById(id);
	}

	@Override
	public void deleteById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateById(int id, String name) {
		dao.updateById(id, name);
	}

	@Override
	public List<Album> getAlbumByArtiestId(int id) {
		return dao.getAlbumByArtiestId(id);
	}

}
