package com.webapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity bean with JPA annotations
 */
@Entity
@Table(name="Album")
public class Album {
	@Id
	@Column(name="album_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int album_id;
	
	private String album_naam;
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="artiest_id")
	private Artiest artiest;

	public int getId() {
		return album_id;
	}

	public void setId(int id) {
		this.album_id = id;
	}

	public String getName() {
		return album_naam;
	}

	public void setName(String name) {
		this.album_naam = name;
	}
	
	public Artiest getArtiest(){
		return artiest;
	}
	
	public void setArtiest(Artiest artiest){
		this.artiest = artiest;
	}
	
	@Override
	public String toString(){
		return "album_id="+album_id+", album_naam="+album_naam+", artiest="+artiest.toString();
	}
}
