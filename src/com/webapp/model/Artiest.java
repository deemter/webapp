package com.webapp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity bean with JPA annotations
 */
@Entity
@Table(name="Artiest")
public class Artiest {
	@Id
	@Column(name="artiest_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int artiest_id;
	private String artiest_naam;
	
	@OneToMany(mappedBy = "artiest", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<Artiest> artiest;

	public int getId() {
		return artiest_id;
	}

	public void setId(int id) {
		this.artiest_id = id;
	}

	public String getName() {
		return artiest_naam;
	}

	public void setName(String name) {
		this.artiest_naam = name;
	}
	
	@Override
	public String toString(){
		return "artiest_id="+artiest_id+", artiest_naam="+artiest_naam;
	}
}
