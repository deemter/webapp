angular.module('F1FeederApp.services', [])
  .factory('ergastAPIservice', function($http) {

    var ergastAPI = {};

    ergastAPI.getArtists = function(name) {
    	if(name == ''){
	      return $http({
	    	  method: 'GET',
	    	  url: 'getArtiest'
	    	});
    	}
    	else{
    		return $http({
	    	  method: 'GET',
	    	  url: 'getArtiest/'+name
	    	});
    	}
    }

    ergastAPI.getArtistById = function(id) {
		return $http({
	    	  method: 'GET',
	    	  url: 'getArtiestById/'+id
		});
    }
    
    ergastAPI.addArtist = function(name) {
		return $http({
    	  method: 'GET',
    	  url: 'createArtiest/'+name
		});
    }
    
    ergastAPI.updateArtist = function(id, name) {
		return $http({
    	  method: 'GET',
    	  url: 'updateArtiest/'+id+'/'+name
		});
    }   
    
    ergastAPI.deleteArtist = function(id) {
    		return $http({
	    	  method: 'GET',
	    	  url: 'deleteArtiest/'+id
    	});
    }
    
    ergastAPI.getAlbums = function(id) {
      return $http({
        method: 'GET', 
        url: 'getAlbum/'+id
      });
    }
    
    ergastAPI.addAlbum = function(id, name) {
      return $http({
        method: 'GET', 
        url: 'createAlbum/'+id+'/'+name
      });
    }

    ergastAPI.updateAlbum = function(id, name) {
		return $http({
    	  method: 'GET',
    	  url: 'updateAlbum/'+id+'/'+name
		});
    }   
    
    ergastAPI.deleteAlbum = function(id) {
		return $http({
    	  method: 'GET',
    	  url: 'deleteAlbum/'+id
		});
    }

    return ergastAPI;
  });