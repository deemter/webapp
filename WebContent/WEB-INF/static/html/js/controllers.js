angular.module('F1FeederApp.controllers', []).

  /* Drivers controller */
  controller('driversController', function($scope, ergastAPIservice) {
    $scope.nameFilter = null;
    $scope.artiestList = [];
    $scope.searchFilter = function (data) {
        var re = new RegExp($scope.nameFilter, 'i');
        return !$scope.nameFilter || re.test(data.name) || re.test(data.name);
    };
    
    $scope.getMyCtrlScope = function() {
        return $scope;   
   };
    
    $scope.deleteArtiest = function(id){
    	ergastAPIservice.deleteArtist(id).then(function (response) {
            $scope.getArtiest();
        });
    };
    
    $scope.addArtiest = function(){
    	var name = document.getElementById("search").value;
        ergastAPIservice.addArtist(name).then(function (response) {
            $scope.getArtiest();
        	console.log(response);
        });
    }
    
    $scope.getArtiest = function(){
    	var name = document.getElementById("search").value;
        ergastAPIservice.getArtists(name).then(function (response) {
            //Digging into the response to get the relevant data
        	console.log(response);
            $scope.artiestList = response.data;
        });
    }
  }).

  /* Driver controller */
  controller('driverController', function($scope, $routeParams, ergastAPIservice) {
    $scope.id = $routeParams.id;
    $scope.races = [];
    $scope.albumList = null;
    $scope.artistName = null;
    $scope.getAlbums = function(){
	    ergastAPIservice.getAlbums($scope.id).then(function (response) {
	        $scope.albumList = response.data;
	        if(response.data){
	        	$scope.albumList = response.data;
	        	$scope.artistName = response.data[0].artiest.name;
	        }
	        else{
	            ergastAPIservice.getArtistById($scope.id).then(function (response) {
	            	console.log(response.data.name);
	            	$scope.artistName = response.data.name;
	            });
	        }
	    });
    }
    
    $scope.deleteAlbum = function(id){
    	ergastAPIservice.deleteAlbum(id).then(function (response) {
            $scope.getAlbums();
        });
    };
    
    $scope.addAlbum = function(){
    	var name = document.getElementById("addAlbum").value;
        ergastAPIservice.addAlbum($scope.id, name).then(function (response) {
            $scope.getAlbums();
            alert("Album created");
        	console.log(response);
        });
    }
    
    $scope.updateAlbum = function(id){
    	var name = document.getElementById(id).value;
    	console.log(document.getElementById(id).value);
        ergastAPIservice.updateAlbum(id, name).then(function (response) {
            alert("Album updated");
        });
    }
    
    $scope.updateArtiest = function(){
    	var name = document.getElementById("artiest_name").value;
    	ergastAPIservice.updateArtist($scope.id, name).then(function (response) {
            $scope.getAlbums();
        });
    }
    
    $scope.getAlbums();
  });